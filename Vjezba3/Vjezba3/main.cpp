#include "funkcije.hpp"

//1.zdk
void vec_input(vector<int>& vect, int n) {
	int unos;
	for (int i = 0; i < n; i++) {
		cin >> unos;
		vect.push_back(unos);
	}
}
void vec_print(vector<int>& vect) {
	for (int i = 0; i < vect.size(); i++) {
		cout << vect.at(i) << endl;
	}
}

//2.zdk
vector<int> vrati(vector<int> v1, vector<int> v2) {
	vector<int> v3;
	for (int i = 0; i < v1.size(); i++) {
		if (!(count(v2.begin(), v2.end(), v1[i]))) {
			v3.push_back(v1[i]);
		}
	}
	return v3;
}
//3.zdk
bool compare(int a, int b) {
	return b > a;
}
void sortiraj(vector<int>& vektor) {
	sort(vektor.begin(), vektor.end(), compare);
	int zbroj = 0;
	for (int x : vektor) {
		zbroj += x;
	}
	vektor.push_back(zbroj);
	vektor.insert(vektor.begin(), 0);
}
//4.zdk
void izbrisi(vector<int>& vektor, int n) {
	for (int x = 0; x < vektor.size();x++) {
		if (vektor.at(x) == n) {
			vektor.erase(vektor.begin() + x);
		}
	}
}
//5.zdk
int ginca(string sen, string a) {
	int brojac = 0;
	for (unsigned int i = 0; i < sen.size(); i++) {
		if (sen.compare(sen.size() - (sen.size() - i), a.size(), a) == 0) {
			brojac++;
		}
	}
	return brojac;

}

//6.zdk
vector<string> unesistringove() {
	string x;
	cout << "unesite broj elemenata: ";
	int n;
	cin >> n;
	vector<string> povrat;
	for (int i = 0; i < n; i++) {
		cin >> x;
		povrat.push_back(x);
	}
	for (int i = 0; i < povrat.size(); i++) {
		reverse(povrat.at(i).begin(), povrat.at(i).end());
	}
	sort(povrat.begin(), povrat.end());
	return povrat;
}
int main() {
//Zad1
	int x=2;
	cout << "Broj elemenata koji unosite: ";
	cin >> x;
	vector<int> vektor;
	vec_input(vektor, x);
	vec_print(vektor);
//zad2
	vector<int> vektor2{ 5,7,6,8,9 };
	vector<int> vektor3 = vrati(vektor, vektor2);
	for (int x : vektor3)
		cout << x << " ";
//zad3
	sortiraj(vektor2);
	for (int x : vektor2)
		cout << x << " ";
//zad4
	izbrisi(vektor2, 6);
	for (int x : vektor2)
		cout << x << " ";
//zad5
	vector<string> vektor3 = { "ginilimunada","gina","hongin" };
	string str = "stabcrstrstr";
	string sub = "str";
	cout << "Ponavlja se: " << ginca(str, sub);
//zad6
	vector<string> vektor4 = unesistringove();
	for (string x : vektor4)
		cout << x << " ";
	return 0;
} //POP
