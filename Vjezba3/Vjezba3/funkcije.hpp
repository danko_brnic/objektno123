#define _CRT_SECURE_NO_WARNINGS
#pragma once
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
//1.zdk
void vec_input(vector<int>& vect, int n);
void vec_print(vector<int>& vect);

//2.zdk
vector<int> vrati(vector<int> v1, vector<int> v2);

//3.zdk
bool compare(int a, int b);
void sortiraj(vector<int>& vektor);

//4.zdk
void izbrisi(vector<int>& vektor, int n);

//5.zdk
int ginca(string sen, string a);

//6.zdk
vector<string> unesistringove();

//popravljeno
