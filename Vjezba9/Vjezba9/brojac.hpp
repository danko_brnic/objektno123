using namespace std;

class brojac : public animal
{
protected:
    int count;

public:
    brojac() { count = 0; }
    int get_legs(animal& a) { return a.get_legs(); }
    void add_animal(animal& a);
    string get_name(animal& a) { return a.get_name(); };
    void print() { cout << count; }
};
