using namespace std;

class spider : public animal
{
protected:
    int legs = 6;
    string name = "Spider";

public:
    int get_legs() { return legs; }
    string get_name() { return name; };
};

class tarantula : public spider
{
protected:
    string name = "Tarantula";

public:
    string get_name() { return name; };
};