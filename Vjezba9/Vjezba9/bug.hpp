using namespace std;

class bug : public animal
{
protected:
    int legs = 8;
    string name = "Bug";

public:
    int get_legs() { return legs; }
    string get_name() { return name; };
};

class zohar : public bug
{
protected:
    string name = "Zohar";

public:
    string get_name() { return name; };
};