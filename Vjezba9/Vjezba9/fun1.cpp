#include "fun1.hpp"

Game::Game()
{
	for (int i = 0; i < 2; i++)
	{
		playerNumber[i].ime = " ";
		playerNumber[i].brojNovcica = 0;
		playerNumber[i].ukupniZbroj = 0;
		playerNumber[i].bodovi = 0;
		playerNumber[i].odabir = 0;

		CPUNumber[i].ime = " ";
		CPUNumber[i].brojNovcica = 0;
		CPUNumber[i].ukupniZbroj = 0;
		CPUNumber[i].bodovi = 0;
		CPUNumber[i].odabir = 0;
	}
}

Player::Player()
{
	string ime = " ";
	int brojNovcica = 0;
	int ukupniZbroj = 0;
	int bodovi = 0;
	int odabir = 0;
}

void Game::modGame()
{
	cout << "Dobrodosli u igru Japaneza!" << endl << endl << "Za igru protiv racunala unesite 1" << endl << "Za igru protiv drugog igraca unesite 2" << endl;
	int mod;
	cin >> mod;

	playerNumber[0].setPlayer();

	if (mod == 1)
	{
		CPUNumber[0].setAI();
	}

	else if (mod == 2)
	{
		playerNumber[1].setPlayer();
	}
}

void ComputerPlayer::setAI()
{
	cout << "Racunalo bira..." << endl << endl;
	//Sleep(4000);
	int rngKune;
	ime = "CPU";
	brojNovcica = rand() % 4;

	if (brojNovcica == 0)
	{
		ukupniZbroj = 0;
	}
	else if (brojNovcica == 1)
	{
		rngKune = rand() % 3;

		if (rngKune == 0)
			ukupniZbroj = 1;

		else if (rngKune == 1)
			ukupniZbroj = 2;

		else if (rngKune == 2)
			ukupniZbroj = 5;
	}
	else if (brojNovcica == 2)
	{
		rngKune = rand() % 3;

		if (rngKune == 0)
			ukupniZbroj = 3;

		else if (rngKune == 1)
			ukupniZbroj = 6;

		else if (rngKune == 2)
			ukupniZbroj = 7;
	}
	else if (brojNovcica == 3)
		ukupniZbroj = 8;

	printAI();
}

void HumanPlayer::setPlayer()  //LOGIKA ZA UNOS
{
	int brojac;
	if (ime == " ")
	{
		cout << "Unesite svoje ime: ";
		cin >> ime;
	}
	cout << endl << "Vi ste na redu!" << endl << endl;
	cout << endl << "Koliko novcica zelite uzeti od maksimalno 3?" << endl;
	cin >> brojNovcica;

	if (brojNovcica == 0)
		ukupniZbroj = 0;

	else if (brojNovcica == 1)
	{
		cout << "Koji novcic zelite uzeti? Imate na raspolaganju kovanice od 1, 2 i 5 kuna" << endl;
		cin >> ukupniZbroj;
	}

	else if (brojNovcica == 2)
	{
		cout << "Koje novcice zelite uzeti? Imate na raspolaganju kovanice od 1, 2 i 5 kuna" << endl;
		cin >> brojac;

		if (brojac == 1)
		{
			cout << "Koje novcice zelite uzeti? Imate na raspolaganju kovanice od 2 i 5 kuna" << endl;
			cin >> ukupniZbroj;
			ukupniZbroj = ukupniZbroj + brojac;
		}

		else if (brojac == 2)
		{
			cout << "Koji novcic zelite uzeti? Imate na raspolaganju kovanice od 1 i 5 kuna" << endl;
			cin >> ukupniZbroj;
			ukupniZbroj = ukupniZbroj + brojac;
		}

		else if (brojac == 5)
		{
			cout << "Koji novcic zelite uzeti? Imate na raspolaganju kovanice od 1 i 2 kuna" << endl;
			cin >> ukupniZbroj;
			ukupniZbroj = ukupniZbroj + brojac;
		}

		else
		{
			cout << "Nazalost nema novcica te vrijednosti";
			exit(1);
		}
	}

	else if (brojNovcica == 3)
	{
		cout << "Uzeli ste sve novcice";
		ukupniZbroj = 8;
	}

	else
	{
		cout << "Nazalost nema tolikog broja novcica";
		exit(1);
	}
	printPlayer();
}

void ComputerPlayer::printAI()  //PRINTA TRENUTNO STANJE
{
	cout << endl << ime << "\t" << brojNovcica << "\t" << ukupniZbroj << endl;
}

void HumanPlayer::printPlayer()
{
	cout << endl << ime << "\t" << brojNovcica << "\t" << ukupniZbroj << endl;
}

void HumanPlayer::guessPlayer()
{
	cout << endl << "Pogodite koliko kuna imate zajedno: ";
	cin >> odabir;
}

void ComputerPlayer::guessAI()
{
	odabir = 12;
	while (odabir == 12)
	{
		odabir = rand() % 17;
	}
}

void Game::checkWinner()
{
	int flag = 0;

	for (int i = 0; i < 2; i++)
	{
		if (playerNumber[i].ime != " ")
		{
			playerNumber[i].guessPlayer();
		}
		if (CPUNumber[i].ime != " ")
		{
			CPUNumber[i].guessAI();
			flag = 1;
		}
	}

	// da je igra u vise igraca ovdje bi isao for sa granicama 1 manje od maksimalnog broja igraca da ne izade iz rangea niza

	if (flag == 0)
	{
		if ((playerNumber[0].odabir == (playerNumber[0].ukupniZbroj + playerNumber[1].ukupniZbroj)) && (playerNumber[1].odabir == (playerNumber[0].ukupniZbroj + playerNumber[1].ukupniZbroj)))
		{
			cout << "Oboje ste pogodili!" << endl;
			playerNumber[0].bodovi++;
			playerNumber[1].bodovi++;
		}
		else if (playerNumber[0].odabir == playerNumber[0].ukupniZbroj + playerNumber[1].ukupniZbroj)
		{
			cout << "Pogodio je prvi igrac!" << endl;
			playerNumber[0].bodovi++;
		}
		else if (playerNumber[1].odabir == playerNumber[0].ukupniZbroj + playerNumber[1].ukupniZbroj)
		{
			cout << "Pogodio je drugi igrac!" << endl;
			playerNumber[1].bodovi++;
		}
		else
		{
			cout << "Nitko nije pogodio!" << endl;
		}

		cout << playerNumber[0].ime << " ima " << playerNumber[0].bodovi << " bodova." << endl;
		cout << playerNumber[1].ime << " ima " << playerNumber[1].bodovi << " bodova." << endl;

		if (playerNumber[0].bodovi != 3 && playerNumber[1].bodovi != 3)
		{
			playerNumber[0].setPlayer();
			playerNumber[1].setPlayer();
			checkWinner();
		}

		else if (playerNumber[0].bodovi == 3 && playerNumber[1].bodovi == 3)
		{
			cout << "Nerijeseno!";
			exit(1);
		}

		else if (playerNumber[0].bodovi == 3)
		{
			cout << "Cestitamo, pobjedio je " << playerNumber[0].ime;
			exit(1);
		}
		else if (playerNumber[1].bodovi == 3)
		{
			cout << "Cestitamo, pobjedio je " << playerNumber[1].ime;
			exit(1);
		}
	}

	else
	{
		if ((playerNumber[0].odabir == playerNumber[0].ukupniZbroj + CPUNumber[0].ukupniZbroj) && (CPUNumber[0].odabir == playerNumber[0].ukupniZbroj + CPUNumber[0].ukupniZbroj))
		{
			cout << "Oboje ste pogodili!" << endl;
			playerNumber[0].bodovi++;
			CPUNumber[0].bodovi++;
		}
		else if (playerNumber[0].odabir == playerNumber[0].ukupniZbroj + CPUNumber[0].ukupniZbroj)
		{
			cout << "Pogodio je prvi igrac!" << endl;
			playerNumber[0].bodovi++;
		}
		else if (CPUNumber[0].odabir == playerNumber[0].ukupniZbroj + CPUNumber[0].ukupniZbroj)
		{
			cout << "Pogodio je drugi igrac!" << endl;
			CPUNumber[0].bodovi++;
		}
		else
		{
			cout << "Nitko nije pogodio!" << endl;
		}

		cout << playerNumber[0].ime << " ima " << playerNumber[0].bodovi << " bodova." << endl;
		cout << CPUNumber[0].ime << " ima " << CPUNumber[0].bodovi << " bodova." << endl;

		if (playerNumber[0].bodovi != 3 && CPUNumber[0].bodovi != 3)
		{
			playerNumber[0].setPlayer();
			CPUNumber[0].setAI();
			checkWinner();
		}

		else if (playerNumber[0].bodovi == 3 && CPUNumber[0].bodovi == 3)
		{
			cout << "Nerijeseno!";
			exit(1);
		}

		else if (playerNumber[0].bodovi == 3)
		{
			cout << "Cestitamo, pobjedio je " << playerNumber[0].ime;
			exit(1);
		}
		else if (CPUNumber[0].bodovi == 3)
		{
			cout << "Zao nam je, izgubili ste";
			exit(1);
		}
	}
}