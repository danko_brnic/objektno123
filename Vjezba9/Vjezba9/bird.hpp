using namespace std;

class bird : public animal
{
protected:
    int legs = 2;
    string name = "Bird";

public:
    int get_legs() { return legs; }
    string get_name() { return name; };
};

class vrabac : public bird
{
protected:
    string name = "Vrabac";

public:
    string get_name() { return name; };
};