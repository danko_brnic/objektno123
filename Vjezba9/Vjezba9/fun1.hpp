#include <iostream>
#include <string>
#include <Windows.h>
#include <time.h>

using namespace std;

class Player
{

public:
	string ime;
	int brojNovcica;
	int ukupniZbroj;
	int bodovi;
	int odabir;
	Player();   //KONSTRUKTOR
	~Player() {};//DESTRUKTOR
};

class HumanPlayer :public Player
{

public:
	void setPlayer();
	void printPlayer();
	void guessPlayer();
};

class ComputerPlayer :public Player
{

public:
	void setAI();
	void printAI();
	void guessAI();
};

class Game
{
private:
	HumanPlayer playerNumber[2];
	ComputerPlayer CPUNumber[2];

public:
	void modGame();
	void checkWinner();
	Game();
	~Game() {};
};