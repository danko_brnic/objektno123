#ifndef __GUARD
#define __GUARD
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
using namespace std;

class timer {
private:
    int hours;
    int minutes;
    double seconds;
    friend class penalty;
public:
    timer();
    timer(int, int, double);
    void round();
    double toseconds();
    timer& operator+=(const timer&);
    timer& operator/=(int);
    friend ostream& operator<<(ostream&, const timer&);
    friend bool operator<(timer&, timer&);
    friend double operator-(timer&, timer&);
};

class penalty {
private:
    int penal;
public:
    penalty(int);
    void operator()(timer&);
};

#endif