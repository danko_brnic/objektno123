#include "fun.hpp"

timer::timer() { hours = 0, minutes = 0, seconds = 0; }
timer::timer(int h, int m, double s) { hours = h, minutes = m, seconds = s; }

void timer::round() {
    while (seconds > 60)
        minutes++, seconds = seconds - 60;
    while (minutes > 60)
        hours++, minutes = minutes - 60;
}

double timer::toseconds() { return (double)(this->hours * 3600 + this->minutes * 60 + this->seconds); }
timer& timer::operator+=(const timer& t) {
    hours += t.hours, minutes += t.minutes, seconds += t.seconds;
    round();
    return *this;
}

timer& timer::operator/=(int x) {
    double seconds = this->toseconds();
    seconds = double(seconds / x), this->hours = seconds / 3600;
    seconds = seconds - hours * 3600, this->minutes = seconds / 60;
    seconds = seconds - minutes * 60, this->seconds = seconds;
    this->round();
    return *this;
}

penalty::penalty(int p) { penal = p; }
void penalty::operator()(timer& t) { t.seconds += penal; }
