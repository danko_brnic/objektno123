#include <iostream>
using namespace std;

struct Point
{
	double x;
	double y;
	Point(); //prazni konstruktor, postavlja vrijednost x i y na 1
	Point(double, double); //postavlja x i y na vrijednosti koje mu se salju
	~Point() {}; //destruktor
};

class Board
{
private:
	int rows;
	int cols;
	char boarder;
	char** matrix;
public:
	Board(); //postavlja vrijednosti row and col na 0
	Board(int, int, char); //postavlja rows and cols na to sta prima 
	Board(const Board&); //copy kontruktor
	~Board(); //destruktor
	void draw_board(); //iscrtava plocu
	void draw_char(Point, char); //za iscrtavanje simbola negdi na ploci
	void display_board(); //za ispisivanje trenutnog stanja ploce
	void draw_up_line(Point, char); //za iscrtavanje linije put gori iz tocke koja se posalje i sad znakom koji se posalje
	void draw_line(Point, Point, char); //za iscrtavanje linijie od tocke do tocke
	int getRows() { return rows; }; //vraca broj redaka
	int getCols() { return cols; }; //vraca broj stupaca
};