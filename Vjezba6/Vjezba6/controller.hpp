#pragma once
#include "model.hpp"
//kopirano sa view.hpp do sljedeceg komentiranja(///)
Hangman player;

void displayRules()
{
    cout << endl;
    cout << "\t\t\t HOW TO PLAY" << endl;
    cout << "\t\t\t-------------" << endl;
    cout << "Welcome to hangman." << endl;
    cout << "You have to guess a movie title." << endl;
    cout << "Each letter is represented by a _." << endl;
    cout << "You have to type only one letter in one try." << endl;
    cout << "You have 8 tries to try and guess the word." << endl;
    cout << "-------------------------------------------------------------" << endl;
    cout << endl;
    cout << "Press enter to continue." << endl;
    cin.ignore();
    cin.get();
}

void displayHangman() {
    switch (player.getLives()) {
    case 0:
        cout << endl << "   _______\n   | /   |\n   |/    O\n   |    /|\\\n   |     |\n   |    / \\\n   |\n___|____    ";
        break;
    case 1:
        cout << endl << "   _______\n   | /   |\n   |/    O\n   |    /|\\\n   |     |\n   |    /\n   |\n___|____    ";
        break;
    case 2:
        cout << endl << "   _______\n   | /   |\n   |/    O\n   |    /|\\\n   |     |\n   |\n   |\n___|____    ";
        break;
    case 3:
        cout << endl << "   _______\n   | /   |\n   |/    O\n   |    /|\\\n   |\n   |\n   |\n___|____    ";
        break;
    case 4:
        cout << endl << "   _______\n   | /   |\n   |/    O\n   |    /|\n   |\n   |\n   |\n___|____    ";
        break;
    case 5:
        cout << endl << "   _______\n   | /   |\n   |/    O\n   |     |\n   |\n   |\n   |\n___|____    ";
        break;
    case 6:
        cout << endl << "   _______\n   | /   |\n   |/    O\n   |\n   |\n   |\n   |\n___|____    ";
        break;
    case 7:
        cout << endl << "   _______\n   | /   |\n   |/\n   |\n   |\n   |\n   |\n___|____    ";
        break;
    default:
        cout << endl << "   _______\n   | /\n   |/\n   |\n   |\n   |\n   |\n___|____    ";
        break;
    }
}

void displayCurrentProgress() {
    if (checkIfGameIsOver() && player.getLives() > 0) {
        displayHangman();
        cout << "\t\t" << player.getGuessMovie() << endl << endl;
        cout << "You have " << player.getLives() << " guesses left." << endl;
        cout << "These are the letters you already used: " << player.getUsedLetters() << endl;
    }
    else if (!checkIfGameIsOver() && player.getLives() > 0) {
        displayHangman();
        cout << "\t" << player.getGuessMovie();
        cout << "\tCongradulations! You got it!" << endl;
    }
    else {
        displayHangman();
        cout << "Sorry, you lose...you've been hanged." << endl;
    }
}
////
char userEntry() {
	char x;
	cout << "Guess a letter:\t";
	cin >> x;
	return toupper(x);
}
bool checkLetter(char X) {
	string temp;
	temp.push_back(X);
	if (player.getMovie().find(temp) != string::npos) {
		player.setGuessMovie(X);
		return true;
	}
	player.setUsedLetters(X);
	return false;
}
void updateLives(bool check) {
	if (check == false) {
		player.setLives(player.getLives() - 1);
	}
}
bool checkIfGameIsOver() {
	if (player.getGuessMovie().find("_") == string::npos) {
		player.setInGame(false);
	}
	else if (player.getLives() == 0) {
		player.setInGame(false);
	}
	return player.getInGame();
}
//kopirano sa model.hpp
void Hangman::setMovie() {
	ifstream file("movie.txt");
	string line;
	srand(time(NULL));
	if (file.is_open()) {
		string buffer;
		int count = 0;
		while (!file.eof() && count < (int)(0 + rand() % 38))
		{
			getline(file, buffer, '\n');
			count++;
		}
		getline(file, line, '\n');
		file.close();
	}
	string temp;
	for (size_t i = 0; i < line.length(); i++) {
		if (isalpha(line.at(i))) {
			temp.push_back((char)toupper(line.at(i)));
		}
		else {
			temp.push_back(line.at(i));
		}
	}
	this->movie = temp;
}
string Hangman::getMovie() const {
	return this->movie;
}
void Hangman::setGuessMovie(string newGuess) {
	this->guessMovie = newGuess;
}
void Hangman::setGuessMovie(char X) {
	string temp;
	temp.push_back(X);
	size_t found = this->movie.find(temp);
	while (found != string::npos) {
		this->guessMovie.replace(found, temp.length(), temp);
		found = this->movie.find(temp, found + 1);
	}
}
string Hangman::getGuessMovie() const {
	return this->guessMovie;
}
void Hangman::setMaskedMovie() {
	string temp;
	for (size_t i = 0; i < this->movie.length(); i++) {
		if (isalpha(this->movie.at(i))) {
			temp.push_back('_');
		}
		else {
			temp.push_back(this->movie.at(i));
		}
	}
	this->maskedMovie = temp;
}
string Hangman::getMaskedMovie() const {
	return this->maskedMovie;
}
void Hangman::setLives(int newLives) {
	this->lives = newLives;
}
int Hangman::getLives() const {
	return this->lives;
}
int Hangman::getMaxLives() const {
	return this->maxLives;
}
void Hangman::setUsedLetters(char X) {
	this->usedLetters.push_back(X);
}
string Hangman::getUsedLetters() const {
	return this->usedLetters;
}
void Hangman::setInGame(bool status) {
	this->inGame = status;
}
bool Hangman::getInGame() {
	return this->inGame;
}
Hangman::Hangman() {
	setMovie();
	setMaskedMovie();
	setGuessMovie(getMaskedMovie());
	setLives(getMaxLives());
	setInGame(true);
}
