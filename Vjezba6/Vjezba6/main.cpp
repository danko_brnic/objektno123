﻿#include "controller.hpp"
#include "model.hpp"

int main()
{
    char x;
    bool flag;
    //displayTitle();
    displayRules();
    displayCurrentProgress();
    while (checkIfGameIsOver()) {
        x = userEntry();
        flag = checkLetter(x);
        updateLives(flag);
        displayCurrentProgress();
    }
    return 0;
}