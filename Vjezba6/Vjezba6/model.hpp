#pragma once
#include <iostream>
#include <string>
#include <string.h>
#include <cstring>
#include <time.h>
#include <fstream>
#include <streambuf>
#define _CRT_SECURE_NO_WARNINGS
using namespace std;
class Hangman {

private:
	string movie; //movie.txt
	string guessMovie; //user's guess
	string maskedMovie; //masked title
	int lives; //user's lives
	int maxLives = 8;
	string usedLetters;
	bool inGame;

public:
	Hangman();
	void setMovie();
	string getMovie() const;
	void setGuessMovie(string newGuess);
	void setGuessMovie(char X);
	string getGuessMovie() const;
	void setMaskedMovie();
	string getMaskedMovie() const;
	void setLives(int newLives);
	int getLives() const;
	int getMaxLives() const;
	void setUsedLetters(char X);
	string getUsedLetters() const;
	void setInGame(bool Status);
	bool getInGame();
	void displayTitle();
	void displayRules();
	void displayCurrentProgress();
	void displayHangman();
};
