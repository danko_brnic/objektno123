#include <iostream>
#include <vector>
#include <string>
template <typename T>
bool less(T a, T b)
{
    return a < b;
}
template <>
bool less(char a, char b)
{
    return tolower(a) < tolower(b);
}
template <typename T>
void swap(T& a, T& b)
{
    T temp;
    temp = a;
    a = b;
    b = temp;
}

template <typename T>
void sort(T* arr, int n)
{
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (less(arr[j], arr[i]))
            {
                swap(arr[i], arr[j]);
            }
        }
    }
}