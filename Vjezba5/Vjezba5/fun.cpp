#include "fun.hpp"
void point::setpoint(const double x, const double y, const double z){ //ova fun nije 100% potribna jer bi trebalo u klasama hardcode i postavit odma tocke na 0
    this->x = 0, this->y = 0, this->z = 0;
    if (x != 0 || y != 0 || z != 0)
        this->x = x, this->y = y, this->z = z;
}
void point::setrandompoint(int min, int max){
    srand(time(NULL));
    this->x = min + rand() % (max - min + 1);
    this->y = min + rand() % (max - min + 1);
    this->z = min + rand() % (max - min + 1);
}
void point::getvalues(double& x, double& y, double& z){
    x = this->x;
    y = this->y;
    z = this->z;
}
double point::distance2d(point t1, point t2) {
    double udaljenost1, udaljenost2, len;
    double vecix = t1.x >= t2.x ? t1.x : t2.x;
    double manjix = t1.x <= t2.x ? t1.x : t2.x;
    double veciy = t1.y >= t2.y ? t1.y : t2.y;
    double manjiy = t1.y <= t2.y ? t1.y : t2.y;
    udaljenost1 = abs(vecix - manjix);
    udaljenost2 = abs(veciy - manjiy);
    len = sqrt((udaljenost1 * udaljenost1) + (udaljenost2 * udaljenost2));
    return len;
}
double point::distance3d(point t1, point t2) {
    double udaljenost1, udaljenost2, udaljenost3, len;
    double vecix = t1.x >= t2.x ? t1.x : t2.x;
    double manjix = t1.x <= t2.x ? t1.x : t2.x;
    double veciy = t1.y >= t2.y ? t1.y : t2.y;
    double manjiy = t1.y <= t2.y ? t1.y : t2.y;
    double veciz = t1.z >= t2.z ? t1.z : t2.z;
    double manjiz = t1.z <= t2.z ? t1.z : t2.z;
    udaljenost1 = abs(vecix - manjix);
    udaljenost2 = abs(veciy - manjiy);
    udaljenost3 = abs(veciz - manjiz);
    len = sqrt((udaljenost1 * udaljenost1) + (udaljenost2 * udaljenost2) + (udaljenost3 * udaljenost3));
    return len;
}
void weapon::reload() {
    this->trenutni_broj_metaka = this->broj_metaka;
}
point weapon::shoot() {
    srand(time(NULL));
    point hitshot;
    this->trenutni_broj_metaka--;
    hitshot.setrandompoint(1, 360);
    return hitshot;
}
int target::hitbox(point hit) {
    if ((hit.x > dl.x && hit.y > dl.y) && (hit.x < gd.x && hit.y < gd.y))
        return 1;
    return 0;
}