//#include "fun.hpp"
#include "fun.cpp"
int main() {
    srand(time(NULL));
    int min = -10, max = 500;  //random pozicija za definiranje mete
    int min2 = 100, max2 = 700;
    int hits1 = 0, hits2 = 0;
    point t1, t2;
    target manualp, autop;
    weapon weapon;
    t1.setrandompoint(min, max);
    t2.setrandompoint(min2, max2);
    cout << "Koordinate su\tX:" << t1.x << "\tY:" << t1.y << "\tZ:" << t1.z << endl;
    cout << "Koordinate su\tX:" << t2.x << "\tY:" << t2.y << "\tZ:" << t2.z << endl;
    cout << "\nUdaljenost izmedu tocki je:" << t1.distance3d(t1, t2) << endl;
    manualp.dl.x = 10, manualp.dl.y = 15;
    manualp.gd.x = 300, manualp.gd.y = 300;
    autop.gd.setrandompoint(1, 360);
    autop.dl.setrandompoint(1, 360);
    while (weapon.trenutni_broj_metaka > 1) {
        weapon.shoot();
        if (manualp.hitbox(weapon.shoot()) == 1)
            hits1++;
        if (autop.hitbox(weapon.shoot()) == 1)
            hits2++;
    }
    cout << "Prva meta je pogodena:" << hits1 << "puta" << endl;
    cout << "Druga meta je pogodena:" << hits2 << "puta" << endl;
    return 0;
}