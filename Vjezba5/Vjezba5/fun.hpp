#pragma once
#include <iostream>
#include <cmath>
#include <ctime>
#include <random>
using namespace std;
#define _CRT_SECURE_NO_WARNINGS_
class point {
public:
    double x, y, z;
    void setpoint(const double x, const double y, const double z);
    void setrandompoint(int min, int max);
    void getvalues(double& x, double& y, double& z);
    double distance2d(point t1, point t2);
    double distance3d(point t1, point t2);
};
class weapon {
public:
    const int broj_metaka = 30;
    int trenutni_broj_metaka = broj_metaka;
    void reload();
    point shoot();
};
class target {
public:
    point dl;
    point gd;
    int hitbox(point hit);
};