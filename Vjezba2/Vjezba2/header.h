#pragma once
#include <iostream>
#include <cstring>
#include <cmath>
using namespace std;

void prvi(int niz[], int& min, int& maks);

int& drugi(int(&niz)[10], int n);

struct pravokutnik {
    float x1, y1, x2, y2;
};
struct kruznica {
    float radius;
    float s1, s2;
};
bool presjek(kruznica K, pravokutnik P);
int trazi_presjek(const pravokutnik P[], int n, const kruznica& K);

struct vector {
    int* elements;
    int logical_size;
    int physical_size;
};
    vector* vector_new();
    void vector_delete(vector* v);
    void vector_push_back(vector* x, const int step);
    void vector_pop_back(vector* v);
    int& vector_front(vector* v);
    int& vector_back(vector* v);
    int vector_size(vector* v);
