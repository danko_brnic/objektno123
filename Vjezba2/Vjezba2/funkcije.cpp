#include "header.h"
using namespace std;

//Prvi zdk
void prvi(int niz[], int& min, int& maks) {
	min = niz[0];
	maks = niz[0];
	for (int i = 0; i < 10; i++) {
		if (niz[i] < min)
			min = niz[i];
		if (niz[i] > maks)
			maks = niz[i];
	}
}

//Drugi zdk
int& drugi(int(&niz)[10], int n) {
	return niz[n];
}


//Treci zdk
bool presjek(kruznica K, pravokutnik P) {
    float dx, dy;
    if (P.x1 * P.x2 <= 0) {
        dx = fabs(P.x2 - P.x1);
    }
    else {
        dx = fabs(-1 * P.x2 - P.x1);
    }
    if (P.y1 * P.y2 <= 0) {
        dy = fabs(P.y2 - P.y1);
    }
    else {
        dy = fabs(-1 * P.y2 - P.y1);
    }
    for (float x = P.x1; x < P.x1 + dx; x++) {
        for (float y = P.y1; y < P.y1 + dy; y++) {
            if (((x - K.s1) * (x - K.s1) + (y - K.s2) * (y - K.s2)) == K.radius * K.radius) {
                return true;
            }
        }
    }
    return false;
}

int trazi_presjek(const pravokutnik P[], int n, const kruznica& K) {
    int br = 0;
    for (int i = 0; i < n; i++) {
        if (presjek(K, P[i])) {
            br++;
        }
    }
    return br;
}


//Cetvrti zdk
vector* vector_new() {
    vector* v = new vector;
    v->elements = NULL;  //nullptr
    v->logical_size = 0;
    v->physical_size = 0;  //init
    return v;
}

void vector_delete(vector* v) {
    delete[] v;
    delete v;
}

void vector_push_back(vector* x, const int step) { //step=novi clan niza
    if (x->physical_size == x->logical_size)
    {
        int* y = new int[x->physical_size * 2];
        memcpy(y, x->elements, x->logical_size);  //memcpy(novo misto /niz/pointer, stari, vel)
        x->physical_size *= 2;
        delete[] x->elements;
        x->elements = y;
    }
    x->elements[x->logical_size++] = step;
}

void vector_pop_back(vector* v) {
    v->elements[v->logical_size--] = 0;
}

int& vector_front(vector* v) {
    return v->elements[0];
}

int& vector_back(vector* v) {
    return v->elements[v->logical_size - 1];
}

int vector_size(vector* v) {
    return v->logical_size;  
}
//logical - broj el u nizu
//fizical - alocirani broj mjesta
