#include "header.h"
#include "funkcije.cpp"


//Prvi zdk
int main() {
	int niz[10] = {0,1,2,3,4,5,6,7,8,9};
	int min;
	int maks;
	prvi(niz, min, maks);
	cout << "najmanji broj je " << min << "najveci broj je " << maks << endl;

//Drugi zdk
	int n;
	cout << "unesite element: " << endl;
	cin >> n;
	int& lvalue = drugi(niz, n);
	lvalue++;
	cout << lvalue << endl;

//Treci zdk
	pravokutnik rect[3];
	rect[0].x1 = 200, rect[0].x2 = 200, rect[0].y1 = 205, rect[0].y2 = 205;
	rect[1].x1 = 0, rect[1].x2 = 5, rect[1].y1 = 4, rect[1].y2 = 6;
	rect[2].x1 = 15, rect[2].x2 = 15, rect[2].y1 = 1, rect[2].y2 = 1;
	kruznica c1;
	c1.radius = 10, c1.s1 = 10, c1.s2 = 10;
	cout << "Broj pravokutnika koji se sjeku je:" << trazi_presjek(rect, 3, c1);

//Cet
	vector* test = vector_new();
	vector_push_back(test, 1);
	vector_push_back(test, 2);
	vector_push_back(test, 3);
	cout << "demo push back" << endl;
	for (int i = 0; i < test->logical_size; i++) {
		cout << "element[" << i + 1 << "] = " << test->elements[i] << endl;
	}
	cout << "front: " << vector_front(test);
	cout << endl << "back: " << vector_back(test);
	cout << endl << "logical size: " << vector_size(test) << " physical: " << test->physical_size << endl;
	vector_pop_back(test);
	cout << endl << "demo pop back" << endl;
	for (int i = 0; i < test->logical_size; i++) {
		cout << "element[" << i + 1 << "] = " << test->elements[i] << endl;
	}
	cout << endl << "logical size: " << vector_size(test) << " physical: " << test->physical_size << endl;
	vector_delete(test);
}


