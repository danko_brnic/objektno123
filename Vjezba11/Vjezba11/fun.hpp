#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <fstream>
using namespace std;

vector<int> read(istream& fp) {
    vector<int> vect;
    istream_iterator<int> is(fp), end;
    copy(is, end, back_inserter(vect));
    vector<int>::iterator i = vect.begin();
    while (i != vect.end()) {
        if ((*i) <= 0)
            throw int();
        i++;
    }
    return vect;
}

void count(vector<int> vect) {
    int brojac = 0;
    for (vector<int>::iterator i = vect.begin(); i != vect.end(); ++i) {
        if ((*i) > 500)
            brojac++;
    }
    cout << "Brojeva vecih od 500 ima :" << brojac << endl;
}

void push(vector<int>& vect) {
    vector<int>::iterator it = vect.begin();
    while (it != vect.end()) {
        if ((*it) < 300)
            it = vect.erase(it);
        else
            it++;
    }
}

void sort(vector<int>& vect) {
    sort(vect.begin(), vect.end(), condition);
    for (int i = 0; i < vect.size(); ++i)
        cout << vect[i] << endl;
}

bool condition(int a, int b) {
    return b > a ? false : true;
}
