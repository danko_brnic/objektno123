#pragma once
#include "fun.hpp"
int main() {
    ifstream file("numbers.txt");
    vector<int> vect;
    if (!(file.is_open())) {
        cout << "ERROR";
        return 0;
    }
    try {
        vect = read(file);
        count(vect);
        cout << "Prije: " << endl;
        for (int i = 0; i < vect.size(); ++i)
            cout << vect[i] << endl;
        push(vect);
        cout << "Posli: " << endl;
        for (int i = 0; i < vect.size(); ++i)
            cout << vect[i] << endl;
        sort(vect);
    }
    catch (int x) {
        cout << "Nije prirodan broj" << endl;
    }
}